# Phase IV

### Name: Joel Uong
1. EID: jtu224
2. Gitlab ID: Uongjo
3. Estimated: 14 hrs
4. Actual: 11 hrs

### Name: Alec Sweet
1. EID: ajs5287
2. Gitlab ID: asweet96
3. Estimated: 13 hrs
4. Actual: 9 hrs

### Name: Malcolm Hess
1. EID: meh3965
2. Gitlab ID: malcolmhess3
3. Estimated: 14 hrs
4. Actual: 10 hrs

### Name: Abhishek Dayal
1. EID: ad28855
2. Gitlab ID: abhishekdayal
3. Estimated: 12 hrs
4. Actual: 9 hrs

### Name: Atharva Pendse
1. EID: aap3469
2. Gitlab ID: atharva747
3. Estimated: 13 hrs
4. Actual: 9 hrs

Git SHA: 48dc674a

https://www.policyand.me/

https://gitlab.com/Uongjo/cs373-idb/pipelines

D3 citations:
Bubble Chart: https://bl.ocks.org/alokkshukla/3d6be4be0ef9f6977ec6718b2916d168
Bar Graph: https://bl.ocks.org/hrecht/f84012ee860cb4da66331f18d588eee3
US Map: http://bl.ocks.org/NPashaP/a74faf20b492ad377312?fbclid=IwAR3ciVSKtJGRsZBqQEOvLNHzwNIkvMf3ylP4ZR9Kk6DpnsA73omtjR-BnRg